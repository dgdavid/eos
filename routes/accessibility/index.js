const express = require('express')
const router = express.Router()

/* GET accessibility page. */
router.get('/', (req, res, next) => {
  res.render('accessibility/index')
})

module.exports = router
