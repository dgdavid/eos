describe('Internal pages search', () => {
  // create the demo element we need to test
  const searchbox = $(
    "<input class='js-icons-search form-control' placeholder='Type to search'>"
  )
  const articleElement = $(
    "<div class='js-label-list'><article>dashboard</article><article>icons</article><article>animation</article></div>"
  )
  const searchEmptyMsg = $(
    "<div class='js-icons-search-empty'>No result found</div>"
  )

  before((done) => {
    // we need to insert searchbox and demo article before the tests so we can test it
    $('body').append(searchbox)
    $('body').append(articleElement)
    $('body').append(searchEmptyMsg)

    done()
  })

  after((done) => {
    $(searchbox).remove()
    $(articleElement).remove()
    $(searchEmptyMsg).remove()
    done()
  })

  describe('On search', () => {
    it('Should list all the items when the search is empty', () => {
      internalPageSearch()
      expect($('.js-label-list article')).to.have.lengthOf(3)
    })
    it('Should filter the list when the search is not empty', () => {
      internalPageSearch('dashboard')
      expect($('.js-label-list article:visible')).to.contain('dashboard')
    })
    it('Should show no result found message when search parameter is not matched', () => {
      internalPageSearch('test')
      expect($('.js-icons-search-empty')).to.have.css('visibility', 'visible')
    })
  })
})
