describe('darkTheme', () => {
  before(() => {
    cy.visit('/')
  })

  context('cookies controller', () => {
    it('should create a cookie on first render with value false', () => {
      cy.getCookie('darkTheme').should('have.property', 'value', 'false')
      cy.get('body').not('have.class', 'dark-theme')
    })

    it('should toggle dark-theem class and cookie value on click', () => {
      cy.get('.js-theme-toggle').click()
      cy.getCookie('darkTheme').should('have.property', 'value', 'true')
      cy.get('body').should('have.class', 'dark-theme')
      cy.get('.js-darktheme-alert').should('be.visible')

      // Toggle back to light theme
      cy.get('.js-theme-toggle').click()
      cy.getCookie('darkTheme').should('have.property', 'value', 'false')
      cy.get('body').not('have.class', 'dark-theme')
      cy.get('.js-darktheme-alert').not('be.visible')
    })
  })
})
