describe('cookie-consent', () => {
  before(() => {
    cy.setCookie(
      'annoucement',
      '{%22wasShown%22:true%2C%22campaing%22:%22SUSE%20rebrand%22}'
    )
    cy.visit('/')
  })

  it('should be visible on window load ', () => {
    cy.get('.cookies-alert').should('be.visible')
  })

  it('should hide the banner onClick and create cookies', () => {
    cy.get('.cookies-alert-buttons > .btn-primary').click()
    cy.get('.cookies-alert').should('not.visible')
    cy.getCookie('acceptance').should('have.property', 'value', 'true')
    cy.getCookie('acceptance-remainder').should(
      'have.property',
      'value',
      'true'
    )
  })
})
