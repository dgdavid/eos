describe('input-text a11y testing', () => {
  before(() => {
    cy.visit('http://localhost:3000/examples/demo-components.html')
    cy.injectAxe()
    cy.wait(600)
  })

  context(
    'Testing all statuses at once for common, shared funtionality',
    () => {
      // Applying a context and run parameters
      it('should comply with wcag21aa', () => {
        cy.onlyOn('development')
        cy.checkA11y('.axe-input-text', {
          run: 'wcag21aa'
        })
      })
    }
  )

  context('Testing disabled status', () => {
    it('should have disable propriety and aria-hidden attributes', () => {
      cy.get(
        '[data-component-name="input-text"][data-component-status="disabled"] input'
      ).each(($item) => {
        cy.wrap($item).invoke('prop', 'disabled').should('eql', true)
      })
    })
  })
})
