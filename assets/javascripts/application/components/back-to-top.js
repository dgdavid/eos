$(function () {
  $(document).scroll(toggleBackToTopButton)
  $('.js-back-to-top').on('click', backToTop)
})

const backToTop = () => {
  $('html, body').animate({ scrollTop: 0 }, 500)
}

const toggleBackToTopButton = () => {
  const position = $(document).scrollTop()

  position > 800
    ? $('.js-back-to-top').stop().fadeIn().css('visibility', 'visible')
    : $('.js-back-to-top').stop().fadeOut().css('visibility', 'hidden')
}
