$(function () {
  // Set dark/light theme based on initial render
  Cookies.get('darkTheme') === 'true'
    ? $('body').addClass('dark-theme')
    : $('body').removeClass('dark-theme')
  Cookies.get('darkTheme') === 'true'
    ? $('.js-darktheme-alert').removeClass('alert-hidden')
    : $('body').addClass('alert-hidden')

  // Theme toggle trigger
  $('.js-theme-toggle').on('click', () => {
    const cookieValue = Cookies.get('darkTheme') === 'true'

    if (cookieValue) {
      Cookies.set('darkTheme', 'false', { expires: 1 })
      $('body').removeClass('dark-theme')
      $('.js-darktheme-alert').addClass('alert-hidden')
    } else if (!cookieValue) {
      Cookies.set('darkTheme', 'true', { expires: 1 })
      $('body').addClass('dark-theme')
      $('.js-darktheme-alert').removeClass('alert-hidden')
    }
    // eslint-disable-line no-undef
    $('.theme-icon').toggleClass('d-none')
  })
})
