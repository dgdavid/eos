function getEOSIconSet (callback) {
  $.when(
    $.ajax({
      url: '/api/gitlab/icons',
      dataType: 'json',
      crossDomain: true,
      error: function (xhr, status, error) {
        console.log('status: ', status)
        console.error(`There was an error in the request: ${error}`)
      }
    })
  ).then(function (gitlabResponse) {
    callback(gitlabResponse)
  })
}
