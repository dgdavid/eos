/* ==========================================================================
   Draggable elements
   ========================================================================== */

/**
 * How to use this helpers:
 *
 * add the class .js-draggable to the container element that will become Draggable
 *
 * then add an children element with the class .js-draggable-trigger
 * this will be the trigger for the dragmove, meaning, it will be the element the
 * user has to press to drag the container
 */

const dragMoveListener = (event) => {
  const { target } = event
  // keep the dragged position in the data-x/data-y attributes
  const x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
  const y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy
  translateElement(target, x, y)
}

const translateElement = (target, x, y) => {
  // setting max displacement for y axis
  if (y > -160 && x < 550) {
    // translate the element
    target.style.webkitTransform = target.style.transform = `translate(${x}px, ${y}px)`

    // update the posiion attributes
    target.setAttribute('data-x', x)
    target.setAttribute('data-y', y)
  }
}

// target elements with the "draggable" class
interact('.js-draggable') // eslint-disable-line no-undef
  .draggable({
    // keep the element within the area of it's parent
    allowFrom: '.js-draggable-trigger',
    restrict: {
      restriction: 'parent',
      endOnly: true,
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
    // enable autoScroll
    autoScroll: false,
    // call this function on every dragmove event
    onmove: dragMoveListener
  })
