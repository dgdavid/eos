$(function () {
  strapiService(appAlertsHandler, `applications { annoucements}`)
})

const appAlertsHandler = (data) => {
  /* eslint-disable no-undef */
  const { alert, modal } = data.data.applications[0].annoucements

  if (alert.isEnabled) {
    const $alertDOM = $('.js-dynamic-global-alert')
    const alertCookie = Cookies.get(`annoucement-global-alert`)
      ? JSON.parse(Cookies.get(`annoucement-global-alert`))
      : undefined // eslint-disable-line no-undef

    if (alertCookie && alertCookie === `eos-${alert.date}`) return
    $alertDOM.addClass(`alert-${alert.type}`)
    $alertDOM.find('.alert-global-desktop').html(alert.content.desktop)
    $alertDOM.find('.alert-global-mobile').html(alert.content.mobile)
    $alertDOM.removeClass('alert-hidden')

    if ($alertDOM) {
      $('.js-global-alert-close').on('click', () => {
        Cookies.set(
          'annoucement-global-alert',
          JSON.stringify(`eos-${alert.date}`),
          { expires: 30 }
        )
      })
    }
  } else {
    $('.js-dynamic-global-alert').remove()
  }

  /* Modal box functionality
  ========================================================================== */
  if (modal && modal.isEnabled) {
    const $modalDOM = $('.js-global-modal')
    const modalCookie = Cookies.get('annoucement-global-modal')
      ? JSON.parse(Cookies.get('annoucement-global-modal'))
      : undefined // eslint-disable-line no-undef

    if (modalCookie && modalCookie === `eos-${modal.date}`) return

    $modalDOM.find('.modal-content').css({
      width: modal.size.width ? modal.size.width : 600,
      height: modal.size.height ? modal.size.height : 480,
      background: `url(${modal.imageURL}) no-repeat center center`,
      'background-size': 'cover'
    })

    $modalDOM.find('.modal-body').on('click', () => {
      window.location.replace(modal.linkURL)
    })

    displayModal($modalDOM)
  }

  function displayModal ($modalDOM) {
    $modalDOM.modal({
      show: true,
      focus: true,
      keyboard: true,
      backdrop: 'static'
    })

    $modalDOM.on('hidden.bs.modal', function (e) {
      Cookies.set(
        'annoucement-global-modal',
        JSON.stringify(`eos-${modal.date}`),
        { expires: 30 }
      )
    })
  }
}

// STRAPI OBJECT TEMPLATE 👇
// {
//   "alert": {
//     "type": "info",
//     "content": {
//       "desktop": " Hello world!  <a href='www.google.com'> www </a>",
//       "mobile": "Alert content on mobile"
//     },
//     "isEnabled": true,
//     "date": "13/02/2021"
//   },
//   "modal": {
//     "imageURL": "https://res.cloudinary.com/eosdesignsystem/image/upload/v1610986961/EOS/Social/rebrand-2021.png",
//     "linkURL": "/colors",
//     "size": {
//       "width": 800,
//       "height": 400
//     },
//     "isEnabled": true,
//     "date": "13/02/2021"
//   }
// }
