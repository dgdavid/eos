/**
 * Filter and count filtered labels
 *
 * Example of usage:
 * In your toolbar, add the class `.js-filter-label` to the button, link, or
 * element that after clicked will filter out the results on screen
 *
 * ======
 * button.js-filter-label
 *   | FILTER_ME  // this name will be the filtering criteria
 *   .badge.js-count-filter-label  // this class is used if you want to have a counter of total ocurrencies
 *     | 1
 * ======

 * the script will count the number of `article` under .js-label-list and place
 * the number in the corresponding `.js-count-filter-label`
 *
 * The `article` elements containing labels inside will be filtered as long as
 * they are children of `.js-label-list`
 *
 * ======
 * .js-label-list
 *   article
 *     h1
 *       | Title
 *     label.js-filter-label
 *       | FILTER_ME
 * =======
 *
 */

$(function () {
  /* When clicking on one of the products labels in the filter */
  $('.js-filter-label').on('click', function () {
    const labelTxt = $(this).clone().children().remove().end().text()
    filterLabelsOut(labelTxt)
  })
  countFilterLabels()
})

const countFilterLabels = () => {
  const $allProducts = $('.js-count-filter-label')
  $.each($allProducts, function (i, v) {
    const productName = $(v).parent().clone().children().remove().end().text()
    const $counterContainer = $(v)
    const $iconsContainer =
      productName !== 'All'
        ? $(`.js-label-list article .js-filter-label:contains(${productName})`)
        : $('.js-label-list article')
    const countIcons = $iconsContainer.length
    $counterContainer.html(countIcons)
  })
}

const filterLabelsOut = (labelTxt) => {
  const $labelContainer = $('.js-label-list article')
  for (let i = 0; i < $labelContainer.length; i++) {
    const $currentContainer = $($labelContainer[i])
      .find('.js-filter-label')
      .map(function () {
        return this.innerText
      })
    const findLabel = $.inArray(labelTxt, $currentContainer)
    if (findLabel > -1 || labelTxt === 'All') {
      $labelContainer[i].style.display = ''
    } else {
      $labelContainer[i].style.display = 'none'
    }
  }
}
